import React, { useState, useEffect } from "react";
import {
  GetLondonCity,
  GetManchesterCity,
  GetBirminghamCity,
  GetGlasgowCity,
  GetLiverpoolCity,
} from "../services/ukCitiesService";
import Cities from "../components/cities";
import Layout from "../components/layout";

const HomePage = () => {
  const [lacWeather, setLacWeather] = useState([]);

  //Fetching List of Uk Cities weather
  const fetchLondonWeather = async () => {
    const londonData = await GetLondonCity();
    const manchesterData = await GetManchesterCity();
    const birminghamData = await GetBirminghamCity();
    const glassgowData = await GetGlasgowCity();
    const liverpoolData = await GetLiverpoolCity();
    //Mergin all the obecjts data into single array
    Promise.all([londonData, manchesterData, birminghamData, glassgowData, liverpoolData]).then(values=> {
      setLacWeather(values);
    })
  };

  useEffect(() => {
    fetchLondonWeather();
  }, []);

  return (
    <>
      <Layout>
        <Cities allCitiesData={lacWeather} />
      </Layout>
    </>
  );
};

export default HomePage;
