import React, {useState, useEffect} from "react";
import Layout from "../components/layout";
import { GetWeatherDetails } from '../services/ukCitiesService';
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import PulseLoader from "react-spinners/PulseLoader";
import CityBg from '../components/citybg';

//Stylings
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  mainContainer: {
    //padding: "0px 0px",
    paddingBottom: '20px'
  },
  paper: {
      padding: '0px 0px',
      margin: '0px'
  },
  detailesTitle : {
    fontSize: '30px',
    color: "#666"
  },
  highLighCity: {
    color: '#000'
  },
  listHeading: {
    borderBottom: '1px solid #ddd',
    padding: '10px 0px',
    backgroundColor: "#000",
    color: "#fff",
    margin: '-10px 0px 0px 0px',
  },
 listDat:{
   padding: '0px 0px 10px 0px'
 },
 mainSection:{
   margin: '10px 0px 0px 0px',
   display: 'block',
   position:'relative',
   zIndex: 10,
   padding: '0px'
 },
 displayImg: {
  position:'relative',
  zIndex: 5
 },
  ListWrapper : {
  listStyle: 'none',
  padding: '0px 10px',
  margin: '0px',
}
}));


//single styles
const itemStyle = {
  display: 'block',
  borderBottom: '1px solid #ddd',
  padding: '10px',
  boxShadow: '0px 0px 10px #ddd'
}
const itemChild = {
  display: 'block',
  padding: '2px',
}

const WeatherDetails = () => {
  const [wData, setWData] = useState({});
  const classes = useStyles();

  //Method to get query params


  // query params
  let query = new URLSearchParams(window.location.search);
  let getCityID = query.get("id");
  let getCount = query.get("cnt");
  let getAppID = query.get("appid");

  const getDetailedWeather = async(city, count, appid) => {
    const dataObj = await GetWeatherDetails(city, count, appid);
    setWData(dataObj);
  }
  //Gettting single city details weather
  useEffect(() => { 
    getDetailedWeather(getCityID, getCount, getAppID);
  }, [getCityID, getCount, getAppID]);


  //extracting objects from the main object
  const {city , list} = wData;

  //Rendering list item
  const RenderListItem = ({listItm}) => {
    return(
      <>
        <li style={itemStyle}>
          <span style={itemChild}><strong>Date</strong> : {listItm?.dt_txt}</span>
          <span style={itemChild}><strong>Sea Level</strong>: {listItm?.main?.sea_level}</span>
          <span style={itemChild}><strong>Temparature</strong> : {listItm?.main?.temp}</span>
        </li>
      </>
    )
  }
  return (
    <>
      <Layout> 
        <Container className={classes.mainContainer} maxWidth="md">
          <h1 className={classes.detailesTitle}>Weather Report </h1>
          <Grid container className={classes.root} spacing={2}>
          
          <Grid xs={12} sm={6} md={8} item><CityBg city={city} className={classes.displayImg} /></Grid>
            <Grid item xs={12} sm={6} md={4} >
              <Paper className={classes.paper}>
                  
                  
                  <div className={classes.mainSection}>
                  <Grid container className={classes.root} spacing={2}>
                      <Grid item xs={12} >
                         <h3 className={classes.listHeading}>{city?.name ? city?.name: <PulseLoader size={5} />} Weather</h3>
                           <ul className={classes.ListWrapper}>
                              {list && list.map((listItm) => <RenderListItem  listItm={listItm} key={listItm.dt} />
                             )}
                           </ul>
                      </Grid>
                  </Grid>
                  </div>
              </Paper>
            </Grid>
          </Grid>
        </Container>
      </Layout>
    </>
  );
};

export default WeatherDetails;
