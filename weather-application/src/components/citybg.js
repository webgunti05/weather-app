

import React from 'react';

const CityBg = ({city, className}) => {
    const srcImg = city?.name === "London" ? "../London_bg.jpg" : city?.name === "Manchester" ? "../Manchester_bg.jpg" : city?.name === "Glasgow" ? "../Glasgow_bg.jpg" : city?.name === "Birmingham" ? "../Birmingham_bg.jpg" : city?.name === "Liverpool" ? "../Liverpool_bg.jpg" : "../image-not-found.jpg";
    return(
        <>
            <img src={srcImg} className={className} alt="Uk" width="100%" />

        </>
    )
}

export default CityBg;