import React, { useState } from "react";
import { useHistory} from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import LocationCityIcon from "@material-ui/icons/LocationCity";
import WbSunnyIcon from "@material-ui/icons/WbSunny";
import CloudIcon from "@material-ui/icons/Cloud";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import PulseLoader from "react-spinners/PulseLoader";
import CityBg from './citybg';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  mainContainer: {
    padding: "50px 0px",
  },
  paper: {
    width: 200,
  },
  control: {
    padding: theme.spacing(2),
  },
  listTitle: {
    textAlign: "left",
    fontSize: "13px",
  },
  innerSpacw: {
    padding: "10px",
  },
  detailsButton : {
      margin: '20px 0px 10px 0px',
  },
  centerLoader:{
   display: 'block',
   margin: '0px auto'
  }
}));

// styles
const IconStyle = {
  fontSize: 20,
  position: "relative",
  top: "5px",
  marginRight: '3px'
};


const Cities = ({ allCitiesData }) => {
  const [spacing] = useState(2);
  const history = useHistory();
  const classes = useStyles();
  let APPID = process.env.REACT_APP_APPID;
  let citiesCount = process.env.REACT_APP_CITIES_COUNT;
  

  const DetailsViewPage = (id) => {
    history.push(`/weather-details/forecast?id=${id}&cnt=${citiesCount}&appid=${APPID}`);
  }

  return (
    <>
      <CssBaseline />
      <Container className={classes.mainContainer} maxWidth="lg">
        <h1 data-testid="main_Heading">Here is the list of five UK cities weather forecast</h1>
        
        <Grid container >
          {allCitiesData && allCitiesData.length > 0 ? (
            <Grid item xs={12}>
            <Grid container justify="center" spacing={spacing}>
              {allCitiesData.map((city) => (
                <Grid key={city?.id} item>
                  <Paper className={classes.paper}>
                    <CityBg className="" city={city}  />
                    <div className={classes.innerSpacw}>
                      <Typography className={classes.listTitle}>
                        <span>
                          <LocationCityIcon style={IconStyle} />
                        </span>
                        <strong>{city.name ? city.name: <PulseLoader size={5} />}</strong>
                      </Typography>
                      <Typography className={classes.listTitle}>
                        <span>
                          <CloudIcon style={IconStyle} />
                        </span>
                        Temparature : {city?.main?.temp ? city?.main?.temp : <PulseLoader size={5} />} &deg;C
                      </Typography>
                      <Typography className={classes.listTitle}>
                        <span>
                          <WbSunnyIcon style={IconStyle} />
                        </span>
                        Sunrise Time :
                        {city?.sys?.sunrise ? new Date(city?.sys?.sunrise * 1000).toLocaleTimeString(
                          "en-IN"
                        ): <PulseLoader size={5} />}
                      </Typography>
                      <Typography className={classes.listTitle}>
                        <span>
                          <WbSunnyIcon style={IconStyle} />
                        </span>
                        Sunset Time :
                        {city?.sys?.sunset ? new Date(city?.sys?.sunset * 1000).toLocaleTimeString(
                          "en-IN"
                        ): <PulseLoader size={5} />}
                      </Typography>
                      <Button onClick={() => DetailsViewPage(city?.id)} variant="contained" color="primary" className={classes.detailsButton}>
                        View Details
                      </Button>
                      
                    </div>
                  </Paper>
                </Grid>
              ))}
            </Grid>
          </Grid>
          ) : (
            <div className={classes.centerLoader}><PulseLoader size={15} /></div>
          )}
          
        </Grid>
      </Container>
    </>
  );
};

export default Cities;
