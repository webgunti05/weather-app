import React from 'react';
import { BrowserRouter } from "react-router-dom";
import { render, cleanup} from '@testing-library/react';
import Header from '../header';

afterEach(cleanup);

describe('Header component renders', () => {
    it('Renders without crashing', () => {
      const { container } = render(<BrowserRouter><Header /></BrowserRouter>);
      expect(container).not.toBeNull();
    });
});


