import React from "react";
import { BrowserRouter } from "react-router-dom";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import WeatherDetails from "../../pages/weather-details";
import fetchMock from "fetch-mock";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
  jest.spyOn(console, "error").mockImplementation(() => {});
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});


let storedData = {
  "city": {
    "id": 2643743,
    "name": "London",
    "coord": {
      "lat": 51.5085,
      "lon": -0.1257
    },
    "country": "GB",
    "population": 0,
    "timezone": 3600,
    "sunrise": 1620533875,
    "sunset": 1620588959
  }
};

describe("Weather details page loads data", () => {
  afterEach(() => {
    fetchMock.restore();
  });
  it("Sholud load the data", async () => {
    

    jest.spyOn(global, "fetch").mockImplementation(() =>
    Promise.resolve({
      json: () => Promise.resolve(storedData)
    })
  );
  
  await act(async () => {
    render(<BrowserRouter><WeatherDetails /></BrowserRouter>, container);
  });

  });
});
