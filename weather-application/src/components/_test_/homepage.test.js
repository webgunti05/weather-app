import React from 'react';
import { BrowserRouter } from "react-router-dom";
import { render, cleanup } from '@testing-library/react';
import HomePage from '../../pages/home';

afterEach(cleanup);

describe('Home  page loads', () => {
    it('Home page page should load with h1 tag', () => {
      const { container, getByTestId } = render(<BrowserRouter><HomePage /></BrowserRouter>);
      const createUserContainer = getByTestId('main_Heading');
      expect(container).not.toBeNull();
      expect(createUserContainer).toBeInTheDocument();
    });
  });