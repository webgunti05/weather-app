import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  logo: {
      color: '#fff',
      textTransform: 'uppercase',
      '& > a' : {
        textDecoration: 'none',
        color: '#fff',
      }
  },
  navItemLink: {
      fontSize: '14px',
    '& > a' : {
      textDecoration: 'none',
      color: '#fff',
    }
}
}));

const Header = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        
        <Toolbar>
        <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <Typography variant="h6" className={classes.logo}>
              <Link to = "/">Weather App</Link>
            </Typography>
          </IconButton>
          <Typography variant="h6" className={classes.navItemLink}>
            <Link to = "/home" >Home</Link>
          </Typography>
          
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Header;
