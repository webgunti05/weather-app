
let apiEndPoint = process.env.REACT_APP_API_URL;
let APPID = process.env.REACT_APP_APPID;
// let lattitude = process.env.REACT_APP_LATT;
// let longitude = process.env.REACT_APP_LON;
//let citiesCount = process.env.REACT_APP_CITIES_COUNT;


//UK CITIES LIST GET CALL
// export const GetUkCitiesList = async() => {
//     try {
//         const response = await fetch(`${apiEndPoint}/find?lat=${lattitude}&lon=${longitude}&cnt=${citiesCount}&appid=${APPID}`,{
//             method: 'GET'
//         });
//         const data = await response.json();
//         return data;
//     } catch (error){
//         console.log(error);
//     }
// }

let londonCity = "London,uk";
let manchesterCity = "Manchester";
let birminghamCity = "Birmingham";
let glasgowCity = "Glasgow";
let liverpoolCity = "Liverpool";
export const GetLondonCity = async() => {
        try {
            const response = await fetch(`${apiEndPoint}/weather?q=${londonCity}&appid=${APPID}`,{
                method: 'GET'
            });
            const data = await response.json();
            return data;
        } catch (error){
            console.log(error);
        }
    }

//Manchester city
export const GetManchesterCity = async() => {
    try {
        const response = await fetch(`${apiEndPoint}/weather?q=${manchesterCity}&appid=${APPID}`,{
            method: 'GET'
        });
        const data = await response.json();
        return data;
    } catch (error){
        console.log(error);
    }
}

//Birmingham city
export const GetBirminghamCity = async() => {
    try {
        const response = await fetch(`${apiEndPoint}/weather?q=${glasgowCity}&appid=${APPID}`,{
            method: 'GET'
        });
        const data = await response.json();
        return data;
    } catch (error){
        console.log(error);
    }
}

//Glasgow  city
export const GetGlasgowCity = async() => {
    try {
        const response = await fetch(`${apiEndPoint}/weather?q=${birminghamCity}&appid=${APPID}`,{
            method: 'GET'
        });
        const data = await response.json();
        return data;
    } catch (error){
        console.log(error);
    }
}

//Liverpool   city
export const GetLiverpoolCity = async() => {
    try {
        const response = await fetch(`${apiEndPoint}/weather?q=${liverpoolCity}&appid=${APPID}`,{
            method: 'GET'
        });
        const data = await response.json();
        return data;
    } catch (error){
        console.log(error);
    }
}

//Get Weather Details 
export const GetWeatherDetails = async(id, count, appid) => {
    try {
        const response = await fetch(`${apiEndPoint}/forecast?id=${id}&cnt=${count}&appid=${appid}`,{
            method: 'GET'
        });
        const data = await response.json();
        return data;
    } catch (error){
        console.log(error);
    }
}