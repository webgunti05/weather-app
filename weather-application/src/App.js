import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import HomePage from './pages/home';
import WeatherDetails from './pages/weather-details';
function App() {
  return (
    <div className="App">
        <Router>
            <Switch>
                <Route path="/" exact component={HomePage} />
                <Route path="/home" component={HomePage} />
                <Route path="/weather-details/:city" exact component={WeatherDetails} />
            </Switch>
        </Router>
    </div>
  );
}

export default App;
